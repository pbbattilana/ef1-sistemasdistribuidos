package py.com.sd.servidorudp.entity;

import java.util.ArrayList;
import java.util.List;

public class Persona {

    private Long cedula;
    private String nombre;
    private String apellido;

    List<Coche> coches;

    public Persona() {
        coches = new ArrayList<Coche>();
    }

    public Persona(Long pcedula, String pnombre, String papellido, List<Coche> coches) {
        this.cedula = pcedula;
        this.nombre = pnombre;
        this.apellido = papellido;

        this.coches = coches;
    }

    public Long getCedula() {
        return cedula;
    }

    public void setCedula(Long cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public List<Coche> getCoches() {
        return coches;
    }

    public void setCoches(List<Coche> coches) {
        this.coches = coches;
    }
}
