package py.com.sd.servidorudp;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import py.com.sd.servidorudp.entity.Coche;
import py.com.sd.servidorudp.entity.Persona;
import py.com.sd.servidorudp.entity.PersonaJSON;


public class MainClass {

    public static void main(String[] a) {
        int puertoServidor = 9876;

        List<Persona> personasList = new ArrayList<>();
//        List<Coche> coches = new ArrayList<>();
//        coches.add(new Coche("ABC123", "Toyota", 22000000));
//        coches.add(new Coche("DFG456", "Hyundai", 62000000));
//        personas.add(new Persona(3393659, "Pepe", "Picapiedras", coches));
//        coches.clear();
//        coches.add(new Coche("JDK118", "Ford", 44000000));
//        coches.add(new Coche("SDK011", "KIA", 56000000));
//        personas.add(new Persona(3393659, "James", "Gosling", coches));
//        coches.clear();

        try {
            DatagramSocket serverSocket = new DatagramSocket(puertoServidor);
            System.out.println("Servidor Examen Final Sistemas Distribuidos - UDP ");

            byte[] receiveData = new byte[1024];
            byte[] sendData = new byte[1024];

            while (true) {
                receiveData = new byte[1024];
                DatagramPacket receivePacket
                        = new DatagramPacket(receiveData, receiveData.length);

                System.out.println("Esperando a algun cliente... ");
                serverSocket.receive(receivePacket);

                System.out.println("________________________________________________");
                System.out.println("Aceptamos un paquete");

                String datoRecibido = new String(receivePacket.getData());
                datoRecibido = datoRecibido.trim();
                System.out.println("DatoRecibido: " + datoRecibido);
                List<Persona> personas = PersonaJSON.stringObjeto(datoRecibido);

                InetAddress IPAddress = receivePacket.getAddress();

                int port = receivePacket.getPort();

                System.out.println("De : " + IPAddress + ":" + port);
                System.out.println("Persona Recibida : " + personas.get(0).getCedula() + ", " + personas.get(0).getNombre() + " " + personas.get(0).getApellido());

                try {
                    personasList = personas;
                    System.out.println("Persona insertada exitosamente en la Base de datos");
                } catch (Exception e) {
                    System.out.println("Persona NO insertada en la Base de datos, razón: " + e.getLocalizedMessage());
                }

                // Enviamos la respuesta inmediatamente a ese mismo cliente
                // Es no bloqueante
                sendData = PersonaJSON.objetoString(masCaro(personas)).getBytes();
                DatagramPacket sendPacket
                        = new DatagramPacket(sendData, sendData.length, IPAddress, port);

                serverSocket.send(sendPacket);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }

    }

    public static Coche masCaro(List<Persona> personas) {
        Coche mayor = null;
        Integer mayorP = 0;
        for (Persona persona : personas) {
            for (Coche coche : persona.getCoches()) {
                if(coche.getMonto() > mayorP){
                    mayor = coche;
                    mayorP = coche.getMonto();
                }
            }
        }
        return mayor;
    }
}
