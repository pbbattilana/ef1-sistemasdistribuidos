package py.com.sd.servidorudp.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class PersonaJSON {

    public static String objetoString(Coche p) {
        JSONObject obj = new JSONObject();
        obj.put("chapa", p.getChapa());
        obj.put("marca", p.getMarca());
        obj.put("monto", p.getMonto());

        return obj.toJSONString();
    }

    public static List<Persona> stringObjeto(String str) throws Exception {
        List<Persona> personas = new ArrayList<>();

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;

        JSONArray msg = (JSONArray) jsonObject.get("personas");
        Iterator<Persona> iterator = msg.iterator();
        while (iterator.hasNext()) {
            personas.add(iterator.next());
        }

        return personas;
    }

}
