package py.com.sd.clienteudp.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class PersonaJSON {

    public static String objetoString(Persona p) {
        JSONObject obj = new JSONObject();
        obj.put("cedula", p.getCedula());
        obj.put("nombre", p.getNombre());
        obj.put("apellido", p.getApellido());
//        obj.put("chapa", p.getCoches().get(0));
//        obj.put("marca", p.getCoches().get(1));
//        obj.put("monto", p.getCoches().get(2));

        JSONArray list = new JSONArray();
        for (Coche temp : p.getCoches()) {
            list.add(temp);
        }
        obj.put("coches", list);
        return obj.toJSONString();
    }

    public static Coche stringObjeto(String str) throws Exception {
        Coche p = new Coche();
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;

        p.setChapa((String) jsonObject.get("chapa"));
        p.setMarca((String) jsonObject.get("marca"));
        Integer monto = Integer.parseInt((String) jsonObject.get("monto"));
        p.setMonto(monto);
        
        return p;
    }

}
