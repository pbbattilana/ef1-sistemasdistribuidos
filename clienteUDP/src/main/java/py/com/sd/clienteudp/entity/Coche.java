package py.com.sd.clienteudp.entity;

/**
 *
 * @author pbbattilana
 */
public class Coche {

    private String chapa;
    private String marca;
    private Integer monto;

    public Coche(String chapa, String marca, Integer monto) {
        this.chapa = chapa;
        this.marca = marca;
        this.monto = monto;
    }
    
    public Coche() {
   
    }

    public String getChapa() {
        return chapa;
    }

    public void setChapa(String chapa) {
        this.chapa = chapa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }
}
