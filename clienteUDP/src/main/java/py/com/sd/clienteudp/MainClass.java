package py.com.sd.clienteudp;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import py.com.sd.clienteudp.entity.Coche;
import py.com.sd.clienteudp.entity.PersonaJSON;
import py.com.sd.clienteudp.entity.Persona;

class MainClass {

    public static void main(String a[]) throws Exception {
        String direccionServidor = "127.0.0.1";

        if (a.length > 0) {
            direccionServidor = a[0];
        }

        int puertoServidor = 9876;

        try {
            BufferedReader inFromUser
                    = new BufferedReader(new InputStreamReader(System.in));
            DatagramSocket clientSocket = new DatagramSocket();

            InetAddress IPAddress = InetAddress.getByName(direccionServidor);
            System.out.println("Intentando conectar a = " + IPAddress + ":" + puertoServidor + " via UDP...");

            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];

            List<Persona> personas = new ArrayList<>();
            List<Coche> coches = new ArrayList<>();
            coches.add(new Coche("ABC123", "Toyota", 22000000));
            coches.add(new Coche("DFG456", "Hyundai", 62000000));
            personas.add(new Persona(3393659l, "Pepe", "Picapiedras", coches));
            coches.clear();
            coches.add(new Coche("JDK118", "Ford", 44000000));
            coches.add(new Coche("SDK011", "KIA", 56000000));
            personas.add(new Persona(3393659l, "James", "Gosling", coches));
            coches.clear();

            System.out.print("Ingrese el número de cédula (debe ser numérico): ");
            String strcedula = inFromUser.readLine();
            Long cedula = 0L;
            try {
                cedula = Long.parseLong(strcedula);
            } catch (Exception e1) {
            }
            System.out.print("Ingrese el nombre: ");
            String nombre = inFromUser.readLine();
            System.out.print("Ingrese el apellido: ");
            String apellido = inFromUser.readLine();
            System.out.print("Ingrese la chapa: ");
            String chapa = inFromUser.readLine();
            System.out.print("Ingrese la marca: ");
            String marca = inFromUser.readLine();
            System.out.print("Ingrese el monto del vehiculo (debe ser numérico): ");
            String strMonto = inFromUser.readLine();
            Integer monto = 0;
            try {
                monto = Integer.parseInt(strMonto);
            } catch (Exception e1) {
            }

            coches.add(new Coche(chapa, marca, monto));
            Persona persona = new Persona(cedula, nombre, apellido, coches);
            coches.clear();

            String datoPaquete = PersonaJSON.objetoString(persona);
            sendData = datoPaquete.getBytes();

            System.out.println("Enviar " + datoPaquete + " al servidor. (" + sendData.length + " bytes)");
            DatagramPacket sendPacket
                    = new DatagramPacket(sendData, sendData.length, IPAddress, puertoServidor);

            clientSocket.send(sendPacket);

            DatagramPacket receivePacket
                    = new DatagramPacket(receiveData, receiveData.length);

            System.out.println("Esperamos si viene la respuesta.");

            clientSocket.setSoTimeout(10000);
            try {
                clientSocket.receive(receivePacket);

                String respuesta = new String(receivePacket.getData());
                Coche presp = PersonaJSON.stringObjeto(respuesta.trim());

                InetAddress returnIPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();

                System.out.println("Respuesta desde =  " + returnIPAddress + ":" + port);
                System.out.println("Coche: "+presp);

            } catch (SocketTimeoutException ste) {

                System.out.println("TimeOut: El paquete udp se asume perdido.");
            }
            clientSocket.close();
        } catch (UnknownHostException ex) {
            System.err.println(ex);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
